import moment from "moment";

export default class Evenement {

    constructor(name, date) {
        this.name = name;
        this.date = date;
    }

    setDate(date) {

        this.date = date;
    }

    getDate() {

        return this.date;
    }

    setName(name) {

        this.name = name;
    }

    getName() {

        return this.name;
    }

    isToday() {

        const today = moment();

        const sameDay = today.day() == this.date.day();
        const sameMonth = today.month() == this.date.month();

        return sameDay && sameMonth;
    }

}