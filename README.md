## Projet calendrier/agenda - Simplon

Réalisation d'un calendrier/agenda.


## User Stories

1. En tant que user, je veux pouvoir visualiser tous mes événements du mois en cours afin de pouvoir m'organiser à court terme.

2. En tant que user, je veux pouvoir créer de nouveaux événements afin de ne pas oublier de futurs rendez-vous

3. En tant que user, je veux pouvoir modifier les événements existants afin de pouvoir annuler ou reporter certains d'entre eux

4. En tant que user , je souhaite  avoir un rappel me sigalant les anniversaires de mes contacts.

   ## Use Cases , Maquette , Class 

   

![UseCaseDiagram1](./Images/UseCaseDiagram1.png)

![Untitled-Diagram-calendar](./Images/ClassDiagram1.png)

![UseCaseDiagram1](./Images/fullCalendar.png)
